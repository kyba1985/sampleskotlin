package kotlinproj.samples.it.samples

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinproj.samples.it.samples.basefromdoc.DocSamplesActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startActivitySampleWanted(DocSamplesActivity::class.java)
    }

    fun startActivitySampleWanted(testCaseAcitivtyName : Class<out Activity>) : Unit // will be used to start activity for test case
    {
        val intent = Intent(this, testCaseAcitivtyName)
        startActivity(intent)
    }



}
