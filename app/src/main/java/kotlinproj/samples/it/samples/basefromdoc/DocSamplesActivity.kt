package kotlinproj.samples.it.samples.basefromdoc

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinproj.samples.it.samples.R

class DocSamplesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_doc_samples)
    }

    override fun onResume() {
        super.onResume()
        startSamplesCode()
    }

    fun startSamplesCode() {

        foo()
        escapeNewLine()
        foo2()
        escapeNewLine()
        foo3()
        escapeNewLine()
        foo4()
        escapeNewLine()
        val obj = InitOrderDemo("test", "parentTest")
        println(obj.toString())
        escapeNewLine()
        loopWithBreak()
        escapeNewLine()
        FirstClass.makeStaticCall()
        escapeNewLine()
        val b = FirstClass()
        b.sampleExt("")
        escapeNewLine()
        val user = User("", 13)
        println(user.name)

        //destructuring constructor data class
        val jane = User("Jane", 35)
        val (name, age) = jane
        println("$name, $age years of age")

        val directionEst = Direction.EAST
        println("direction: ${directionEst.name}")

        //use singleton method
        DataProviderManager.registerDataProvider()

        //default value in function with name parameters provided
        A().foo(baz = 1) // The default value bar = 0 is used

        //use spread *a to pass array as T
        val a = arrayOf(1, 2, 3)
        val list = asList(-1, 0, *a, 4)

        //use infix function
        1 shll 2
        1.shll(2)

        //high order function , pass function inside function
        //fold sample
        val items = listOf(1, 2, 3, 4, 5)
        // Lambdas are code blocks enclosed in curly braces.
        items.fold(0, {
            // When a lambda has parameters, they go first, followed by '->'
            acc: Int, i: Int ->
            print("acc = $acc, i = $i, ")
            val result = acc + i
            println("result = $result")
            // The last expression in a lambda is considered the return value:   NB dentro una lambda non posso chiamare return perche fa riferimento alla funziona sopra , prestare attenzioni ai modi con cui si puo gestire questa cosa (tag o altro sulla doc)
            result
        })

        //lambda as last parameter can be omitted in parentesys and go in bracket at the end
        val product = items.fold(1) { acc, e -> acc * e }

        //NB lambda with one parameter and at the end of function can be omitted the paramter and use it for reference the param
        //example (int) -> Boolean
        var itemsInt = ArrayList<Int>(5)
        itemsInt.filter { it > 10 }

        //NB2 returing value from lambda , or normal mode the last code in the lambda is the value returned
        itemsInt.filter {
            val shouldFilter = it > 0
            return@filter shouldFilter //without tag return is reference the outer function not the lambda
        }

        //NB3 ci sono molte funzioni interessanti sulle collections in collections.kt

        //anonymous function
        itemsInt.filter(fun(item) = item > 0)

        //closure
        var sum = 0
        itemsInt.filter { it > 0 }.forEach {
            sum += it
        }
        print(sum)

        //receiver used
        val sumFunctionReceiver: Int.(Int) -> Int = { other -> plus(other) }
        val sumRes = 1.sumFunctionReceiver(4)

        val sum2 = fun Int.(other: Int): Int = this + other //this used explicit


        //esempio interessante di utilizzo di receiver e labda
        class HTML {
            fun body() {}
        }

        fun html(init: HTML.() -> Unit): HTML {
            val html = HTML() // create the receiver object
            html.init() // pass the receiver object to the lambda
            return html
        }
        html {
            // lambda with receiver begins here
            body() // calling a method on the receiver object
        }

        //use inline
        plus(3, 3)
        plus(3, 3.5)



    }

    //use of type function for new function
    val clickHandlerFun: ClickHandler = fun(x: Int, y: String): Boolean { return false } //usato una funzione, potevo usare una lambda
    val clickHandlerFun2: ClickHandler = { _, _ -> false } //usato una lambda che come ultima riga di codice da il ritorno alla funzione

    //instance function type with receiver A.(B)
    private val typeInstanceWithReceiver: TypeFunWithReceiver = { x: Int ->
        var returnVal = false
        if (x > 0)
            returnVal = true
        returnVal
    }
    val resultFun = "test".typeInstanceWithReceiver(3)

    //altro modo particolare di una classe che implementa una interfaccia con un solo metodo
    val intFunction: (Int) -> Int = IntTransformer()
    val result2 = intFunction.invoke(2)

    val another = { i: Int -> i + 1 } //istanza in una funzione , il tipo viene capito dal compilatore
    val result = another.invoke(3)

    //altro esempio con funzioni un po piu complesso con funzioni con receiver
    val repeat: String.(Int) -> String = { times -> repeat(times) }
    val twoParameters: (String, Int) -> String = repeat // OK
    fun runTransformation(f: (String, Int) -> String): String {
        return f("hello", 3)
    }

    val resultNew = runTransformation(repeat) // OK

    //istanza tipo funzione usando ::
    val stringPlus: (String, String) -> String = String::plus
    val resultString = stringPlus.invoke("test", "prova")

    //lambda function instance
    val sum = { x: Int, y: Int -> x + y }
    val resultSum = sum(2, 3)


}
