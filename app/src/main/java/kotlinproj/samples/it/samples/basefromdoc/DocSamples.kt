package kotlinproj.samples.it.samples.basefromdoc

import android.util.Log
import kotlin.properties.Delegates
import kotlin.reflect.KProperty

//costant
const val SUBSYSTEM_DEPRECATED: String = "This subsystem is deprecated"

fun escapeNewLine() {
    println("********")
}

fun <T> println(value: T): Unit {
    Log.d("LOGV", value.toString())
}

fun <T> print(value: T): Unit {
    Log.d("LOGV", value.toString())
}

//label return inner function , similar to continue

fun foo() {
    listOf(1, 2, 3, 4, 5).forEach lit@{
        if (it == 3) return@lit // local return to the caller of the lambda, i.e. the forEach loop
        print(it)
    }
    print(" done with explicit label")
}

// implicit label return , similar to continue

fun foo2() {
    listOf(1, 2, 3, 4, 5).forEach {
        if (it == 3) return@forEach // local return to the caller of the lambda, i.e. the forEach loop
        print(it)
    }
    print(" done with implicit label")
}

// anonymous function return , the return without label is good because is used anonymous function not the lambda function , similar to continue
fun foo3() {
    listOf(1, 2, 3, 4, 5).forEach(fun(value: Int) {
        if (value == 3) return // local return to the caller of the anonymous fun, i.e. the forEach loop
        print(value)
    })
    print(" done with anonymous function")
}

//return like break the for inner with this , break is not available in kotlin in inner loop , this simulated a break

fun foo4() {
    run loop@{
        listOf(1, 2, 3, 4, 5).forEach {
            if (it == 3) return@loop // non-local return from the lambda passed to run
            print(it)
        }
    }
    print(" done with nested loop")
}


fun loopWithBreak() { //can switch break with continue
    loop@ for (i in 1..100) {
        for (j in 1..10) {
            if (j > 5) break@loop
            println("val = $j")
        }
    }
}

//object define , no code in primary constructor , but in init block

/* NOTE: On the JVM, if all of the parameters of the primary constructor have default values, the compiler will generate an
additional parameterless constructor which will use the default values. This makes it easier to use Kotlin with libraries such
as Jackson or JPA that create class instances through parameterless constructors.*/
class Customer(val customerName: String = "")


open class InitOrderDemo(val name: String) { //se name non fosse con val potevo usarla per creare delle proprieta o altro negli init ma il parametro passato non era direttamente un valore nelle proprieta
    val firstProperty = "First property: $name".also(
            ::println
    )

    init {
        kotlin.io.println("First initializer block that prints ${name}")
    }

    val secondProperty = "Second property: ${name.length}".also(::println)

    init {
        kotlin.io.println("Second initializer block that prints ${name.length}")
    }

    var parentName: String? = null

    //maybe a second constructor
    constructor(name: String, parent: String) : this(name) {
        parentName = "$parent $name"
    }

    override fun toString(): String {
        return "InitOrderDemo(name='$name', firstProperty='$firstProperty', secondProperty='$secondProperty', parentName=$parentName)"
    }
}

//template property
/*
var <propertyName>[: <PropertyType>] [= <property_initializer>]
[<getter>]
[<setter>]
*/

//override property
open class Foo {
    open fun f() {}
    open var x: Int? = 0
        get() {
            return 1
        }
        set(value) {
            field = value?.plus(1)
        }
}

class Bar1 : Foo() {
    override var x: Int? = 1
}

//access from inner class with label for class type

class Bar : Foo() {
    override fun f() { /* ... */
    }

    override var x: Int? = 1
        get() = 0

    inner class Baz {
        fun g() {
            super@Bar.f() // Calls Foo's implementation of f()
            kotlin.io.println(super@Bar.x) // Uses Foo's implementation of x's getter
        }
    }
}

//companion object , used for static behavior in java or c#
//lateinit property usage sample

open class FirstClass constructor() {
    var samplePro: Int = 1
    lateinit var subject: Bar

    init {
        if (this::subject.isInitialized) {
            println(FirstClass::subject)
        } else
            subject = Bar()
    }

    //sample of lazy property only read
    val lazyValue: String by lazy {
        println("computed!")
        "Hello"
    }

    //sample use of backing field
    var counter = 0 // Note: the initializer assigns the backing field directly
        set(value) {
            if (value >= 0) field = value
        }

    //another sample property
    private var _table: Map<String, Int>? = null
    public val table: Map<String, Int>
        get() {
            if (_table == null) {
                _table = HashMap() // Type parameters are inferred
            }
            return _table ?: throw AssertionError("Set to null by another thread")
        }

    companion object {
        fun makeStaticCall() {
            kotlin.io.println("static call")
        }
    }

    //multiple interface implementation in class
    interface A {
        fun foo() {
            print("A")
        }

        fun bar()
    }

    interface B {
        fun foo() {
            print("B")
        }

        fun bar() {
            print("bar")
        }
    }

    class C : A {
        override fun bar() {
            print("bar")
        }
    }

    class D : A, B {
        override fun foo() {
            super<A>.foo()
            super<B>.foo()
        }

        override fun bar() {
            super<B>.bar()
        }
    }

}

//extension functions , are expensive use with caution
fun MutableList<Int>.swap(index1: Int, index2: Int) {
    val tmp = this[index1] // 'this' corresponds to the list
    this[index1] = this[index2]
    this[index2] = tmp
}

fun FirstClass.sampleExt(param1: String) {
    Log.d("test", this.counter.toString() + param1)
}

//extension property
val <T> List<T>.lastIndex: Int
    get() = this.size - 1

//data classes use to hold only data , need more deep details for work with library network like retrofit moshi and others
data class User(val name: String, val age: Int)

//NB The standard library provides Pair and Triple . In most cases, though, named data classes are a better design choice,
//because they make the code more readable by providing meaningful names for properties.


/*
Sealed classes are used for representing restricted class hierarchies, when a value can have one of the types from a limited set,
but cannot have any other type. They are, in a sense, an extension of enum classes: the set of values for an enum type is also
restricted, but each enum constant exists only as a single instance, whereas a subclass of a sealed class can have multiple
instances which can contain state.
*/
sealed class Expr

data class Const(val number: Double) : Expr()
data class Sum(val e1: Expr, val e2: Expr) : Expr()
object NotANumber : Expr()


//OUT modifier for generics in kotlin , posso andare in su nella gerarchia con le assegnazioni di classi

interface Source<out T> {
    fun nextT(): T
}

fun demo(strs: Source<String>) {
    val objects: Source<Any> = strs // This is OK, since T is an OUT-parameter
}


//IN modifier for generics in kotlin , posso andare in giu nella gerarchia con le assegnazioni di classi

interface Comparable<in T> {
    operator fun compareTo(other: T): Int
}

fun demo(x: Comparable<Number>) {
    x.compareTo(1.0) // 1.0 has type Double, which is a subtype of Number
// Thus, we can assign x to a variable of type Comparable<Double>
    val y: Comparable<Double> = x // OK!
}

//The Existential Transformation: Consumer in, Producer out! :-)


//Type projections see the docs page 87 for more details

fun copy(from: Array<out Any>, to: Array<Any>) {
// ...
}
//corresponds to Java's Array<? extends Object> , but in a slightly simpler way.


fun fill(dest: Array<in String>, value: String) {
// ...
}
//Array<in String> corresponds to Java's Array<? super String> , i.e. you can pass an array of CharSequence or an array of
//Object to the fill() function.

//Star-projections page 89 more details Foo<*>


//Generic constraints

fun <T : Comparable<T>> sort(list: List<T>) {
// ...
}

// 2 upper bound for generics constrants

fun <T> copyWhenGreater(list: List<T>, threshold: T): List<String>
        where T : CharSequence,
              T : Comparable<T> {
    return list.filter { it > threshold }.map { it.toString() }
}

//anonymous innter classes

//window.addMouseListener(object: MouseAdapter() {
//    override fun mouseClicked(e: MouseEvent) {
// ...
//    }
//    override fun mouseEntered(e: MouseEvent) {
// ...
//    }
//})

//If the object is an instance of a functional Java interface (i.e. a Java interface with a single abstract method), you can create it
//using a lambda expression prefixed with the type of the interface:
//val listener = ActionListener { println("clicked") }


//enum class

enum class Direction {
    NORTH, SOUTH, WEST, EAST
}

enum class Color(val rgb: Int) {
    RED(0xFF0000),
    GREEN(0x00FF00),
    BLUE(0x0000FF)
}


//create an object anonymous on the fly
fun fooObjAnonymous() {
    val adHoc = object {
        var x: Int = 0
        var y: Int = 0
    }
    print(adHoc.x + adHoc.y)
}

//access data in function from anonymous object
/*
fun countClicks(window: JComponent) {
    var clickCount = 0
    var enterCount = 0
    window.addMouseListener(object : MouseAdapter() {
        override fun mouseClicked(e: MouseEvent) {
            clickCount++
        }
        override fun mouseEntered(e: MouseEvent) {
            enterCount++
        }
    })
// ...
}
*/

//singleton declaration
object DataProviderManager {
    fun registerDataProvider() {
// ...
    }

    val allDataProviders: Collection<String>
        get() = ArrayList<String>(3)
}

//for use DataProviderManager.registerDataProvider(...)

//interface and companion object that can implement interface
interface Factory<T> {
    fun create(): T
}

class MyClass {
    companion object : Factory<MyClass> {
        override fun create(): MyClass = MyClass()
    }
}

//delegation pattern when avoid inerhitance , using BY
interface Base {
    fun print()
    fun printMessageLine()
}

class BaseImpl(val x: Int) : Base {
    override fun print() {
        print(x)
    }

    override fun printMessageLine() {}
}

class Derived(b: Base) : Base by b

fun main(args: Array<String>) {
    val b = BaseImpl(10)
    Derived(b).print()
}

//dove vuoi overridare la delegation puoi farlo con override sul metodo
class DerivedA(b: Base) : Base by b {
    override fun printMessageLine() {
        print("abc")
    }
}

//delegate property
class Example {
    var p: String by Delegate()
}

class Delegate {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        return "$thisRef, thank you for delegating '${property.name}' to me!"
    }

    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: String) {
        println("$value has been assigned to '${property.name}' in $thisRef.")
    }
}

//lazy and observable property
class LayClass {
    val lazyValue: String by lazy {
        //solo per val
        println("computed!") //computed viene stampato solo la prima volta
        "Hello"
    }

    var name: String by Delegates.observable("<no name>") { prop, old, new ->
        println("$old -> $new")
    }
}

/*By default, the evaluation of lazy properties is synchronized: the value is computed only in one thread, and all threads will see
the same value. If the synchronization of initialization delegate is not required, so that multiple threads can execute it
simultaneously, pass LazyThreadSafetyMode.PUBLICATION as a parameter to the lazy() function. And if you're sure that the
initialization will always happen on a single thread, you can use LazyThreadSafetyMode.NONE mode, which doesn't incur any
thread-safety guarantees and the related overhead.*/

//function with default values

open class A {
    open fun foo(i: Int = 10) {}
    fun foo(bar: Int = 0, baz: Int) { /* ... */
    }
}

//vairable number paramters in function
fun <T> asList(vararg ts: T): List<T> {
    val result = ArrayList<T>()
    for (t in ts) // ts is an Array
        result.add(t)
    return result
}

//infix notation

/*
Functions marked with the infix keyword can also be called using the infix notation (omitting the dot and the parentheses for
the call). Infix functions must satisfy the following requirements:
They must be member functions or extension functions;
They must have a single parameter;
The parameter must not accept variable number of arguments and must have no default value.
*/

infix fun Int.shll(x: Int) = x + 10


//local functions
class Graph {

}

class Vertex {
    open fun add(vertex: Vertex): Boolean {
        return false
    }
}

fun dfs(graph: Graph) {
    //creazione di una funzione locale
    fun dfs(current: Vertex, visited: Set<Vertex>) {
        if (!visited.add(current)) return
//        for (v in current.neighbors)
//            dfs(v, visited)
    }
//    dfs(graph.vertices[0], HashSet())
}

private fun <E> Set<E>.add(current: E): Boolean {
    return true
}


//local function can access outer variable
/*
fun dfs(graph: Graph) {
    val visited = HashSet<Vertex>()
    fun dfs(current: Vertex) {
        if (!visited.add(current)) return
        for (v in current.neighbors)
            dfs(v)
    }
    dfs(graph.vertices[0])
}*/


//tail recursion , behind is a loop based efficient version used
tailrec fun findFixPoint(x: Double = 1.0): Double = if (x == Math.cos(x)) x else findFixPoint(Math.cos(x))


/*
To be eligible for the tailrec modifier, a function must call itself as the last operation it performs. You cannot use tail recursion
when there is more code after the recursive call, and you cannot use it within try/catch/finally blocks. Currently tail recursion is
only supported in the JVM backend.
*/


//high order function
//sample function fold often used
fun <T, R> Collection<T>.fold(
        initial: R,
        combine: (acc: R, nextElement: T) -> R
): R {
    var accumulator: R = initial
    for (element: T in this) {
        accumulator = combine(accumulator, element)
    }
    return accumulator
}

//function TYPES , definizione del tipo di funzione , come si definisce INT o String
//esempio con in aggiunta un typedef come alias
typealias ClickHandler = (Int, String) -> Boolean

//type with receiver
typealias TypeFunWithReceiver = String.(Int) -> Boolean

//altro modo alternativo particolare per istanziare una variabile di tipo funzione
class IntTransformer : (Int) -> Int {
    override operator fun invoke(x: Int): Int = TODO()
}

//inline function , used for flexible flow control and for remove penalty of high order function with lambda , each function is an object
inline fun <T : Number> plus(integer: Int, other: T): T {
    other.toInt().plus(integer)
    return other
}

